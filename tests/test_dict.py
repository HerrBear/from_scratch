﻿
def hello_from_dict():
    fr2en = {
        "salut": "hello",
        "monde": "world",
    }
    en2fr = {
        "hello": "salut",
        "world": "monde",
    }

    assert fr2en[en2fr["hello"]] == "hello"
    assert fr2en[en2fr["world"]] == "world"

def hello_from_api():
    import requests, json
    url = requests.get("https://api.dictionaryapi.dev/api/v2/entries/en/hello")
    json_text = url.text
    data = json.loads(json_text)
    s = data[0]['word']

    url = requests.get("https://api.dictionaryapi.dev/api/v2/entries/en/world")
    json_text = url.text
    data = json.loads(json_text)
    s += data[0]['word']

    assert s == "helloworld", "Didn't fetch the correct words. Is the API online?"

def test_download_img():
    url = "https://www.python.org/static/img/python-logo@2x.png"
    download_img(url)

def download_img(url, out_name=""):
    import requests, os
    logo_python = requests.get(url)
    if not(out_name):
        out_name = url.split("/")[-1]
    open(os.path.join(os.path.dirname(os.path.abspath(__file__)),
        "img/"+out_name), 'wb').write(logo_python.content)

def test_download_file():
    url = "https://www.python.org/static/img/python-logo@2x.png"
    download_file(url)

def download_file(url, out_dir="downloaded", out_name=""):
    import requests, os
    logo_python = requests.get(url)
    if not(out_name):
        name = url.split("/")[-1]
    open(os.path.join(os.path.dirname(os.path.abspath(__file__)), out_dir+"/"+name), 'wb').write(logo_python.content)


def rajout_test():
    print("HELLO?")

###################################
########   MAIN & TESTS   #########
###################################

linker_tests = {
    'HELLO FROM DICT': hello_from_dict,
    'HELLO FROM API': hello_from_api,
    'JE RAJOUTE UN TEST': rajout_test,
    'DOWNLOAD IMG': test_download_img,
    'DOWNLOAD FILE': test_download_file,
}

IN_LOC = 0
OUT_LOC = 1


def run_tests(ins_outs, func):

    failed_tests = []
    for i in range(len(ins_outs)):
        one_in = ins_outs[i][IN_LOC]
        one_out = ins_outs[i][OUT_LOC]
        try:
            assert func(*one_in) == one_out, "Failed {}".format(i)
        except AssertionError as e:
            failed_tests.append((i, e))

    if len(failed_tests):
        error_msg = "{} tests failed. Error messages listed here:\n".format(len(failed_tests))
        for i in range(len(failed_tests)):
            test = failed_tests[i]
            error_msg += "\t Test {} -\t In: {} _*_ Out: {} _*_ Err: {}\n".format(
                test[0], ins_outs[test[0]][IN_LOC], ins_outs[test[0]][OUT_LOC], test[1]
            )
        raise AssertionError(error_msg)


# FOR TESTING PURPOSES
if __name__ == '__main__':
    print(__name__, __file__)
    import os
    import os as truc
    print(truc.__name__, truc.__file__)
    print(os.__name__, os.__file__)

    print(os.path.dirname(os.path.abspath(__file__)))
    print(os.path.abspath(__name__))

    n_failed = 0
    for test_name, test in linker_tests.items():
        try:
            test()
            print('TEST {} succeeded.'.format(test_name))
        except AssertionError as e:
            print('TEST {} failed. Error returned : {}'.format(test_name, e))
            n_failed += 1
    print('{} failed tests'.format(n_failed))




