﻿def my_first_test():
    assert(0 == 0), "Wow, if that doesn't work, I don't know what will"

###################################
########   MAIN & TESTS   #########
###################################

linker_tests = {
    'FIRST TEST': my_first_test,
}

IN_LOC = 0
OUT_LOC = 1


def run_tests(ins_outs, func):

    failed_tests = []
    for i in range(len(ins_outs)):
        one_in = ins_outs[i][IN_LOC]
        one_out = ins_outs[i][OUT_LOC]
        try:
            assert func(*one_in) == one_out, "Failed {}".format(i)
        except AssertionError as e:
            failed_tests.append((i, e))

    if len(failed_tests):
        error_msg = "{} tests failed. Error messages listed here:\n".format(len(failed_tests))
        for i in range(len(failed_tests)):
            test = failed_tests[i]
            error_msg += "\t Test {} -\t In: {} _*_ Out: {} _*_ Err: {}\n".format(
                test[0], ins_outs[test[0]][IN_LOC], ins_outs[test[0]][OUT_LOC], test[1]
            )
        raise AssertionError(error_msg)


if __name__ == '__main__':
    n_failed = 0
    for test_name, test in linker_tests.items():
        try:
            test()
            print('TEST {} succeeded.'.format(test_name))
        except AssertionError as e:
            print('TEST {} failed. Error returned : {}'.format(test_name, e))
            n_failed += 1
    print('{} failed tests'.format(n_failed))
