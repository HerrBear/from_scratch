﻿#!/usr/bin/bash

PYTHON_CMD=python3
LOG_NAME=tests.log

if [ -f "$LOG_NAME" ]; then
	rm $LOG_NAME
	touch $LOG_NAME
fi

for f in *.py; do
	echo "$f" >> $LOG_NAME
	$PYTHON_CMD $f >> $LOG_NAME
done
