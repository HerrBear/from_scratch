﻿s = "Hello World"   # Chaîne de caractère
print(s)            # Affichage de la string

a = 5.3             # float
print('\n', a)      # Affichage du float en sautant une ligne AVANT

def func(a, b):     # Création de fonction
    # ATTENTION L'INDENTATION EST IMPORTANTE
    print(a+b)      # Affichage avec les paramètres de la fonction

func(1, 3)

class Chair():
    assise = 'assise'
    pieds = 3
    dossier = 'dossier'

chair = Chair()
print(chair.pieds, "pieds")

class MyInt():
    val = 0

    def __init__(self, val):
        self.val = val

    def __add__(self, other):
        print("ADD A BIEN ETE APPELE")
        return self.val + other

print(MyInt("pouet") + "truc" )
a = 3
print(dir(a))
s = "Hello"
print(dir(s))

words = ["Hello", "World", "I", "AM", "ALIVE"]
for word in words:
    print(word)
print("\n")
for i in range(0, len(words), 1):  # for i in range(len(words))
    print(words[i])

for i in range(len(words)-1, -1, -1):
    print(words[i])

words.reverse()
for word in words:
    print(word)

print("********************")
[print(word) for word in words] # Comprehension de list (list comprehension)
inverted_words = [words[i] for i in range(len(words)-1, -1, -1)]
print(inverted_words)

tuple_words = [
    words[i] + " " + words[i+1]
    for i in range(0, len(words) - 1, 1)
]
tuple_words = []
for i in range(0, len(words) - 1, 1):
    tuple_words.append(words[i] + " " + words[i+1])
print(tuple_words)

print("********************")

import os
curr_dir = os.path.dirname(os.path.abspath(__file__))
os_dir = os.path.dirname(os.path.abspath(os.__file__))
work_dir = os.getcwd()
print(curr_dir, work_dir, os_dir)

ROOT_NAME = "from_scratch"
print(curr_dir.split(ROOT_NAME))
ROOT_DIR = os.path.join(curr_dir.split(ROOT_NAME)[0], ROOT_NAME)
DATA_DIR = os.path.join(ROOT_DIR, "data")
TESTS_DIR = os.path.join(ROOT_DIR, "tests")
print(ROOT_DIR, DATA_DIR, TESTS_DIR)

INFO_FN = "info.csv"
my_csv = open(os.path.join(DATA_DIR, INFO_FN), "r", encoding="utf-8")
for line in my_csv.readlines():
    print(line)
my_csv.close()

my_csv = open(os.path.join(DATA_DIR, INFO_FN), "r", encoding="utf-8")
lines = my_csv.readlines()
rows = [line.split(";") for line in lines]
header = rows[0]
del rows[0]
print(rows)
print(header)
print(rows[1:3])
my_csv.close()

print("********************")

MOVIE_FN = "myData.csv"
my_csv = open(os.path.join(DATA_DIR, MOVIE_FN), "r", encoding="utf-8")
lines = my_csv.readlines()
rows = [line.split(";") for line in lines]
header = rows[0]
del rows[0]
print(header)
print(rows[1:3])
my_csv.close()

print("*********************")
d = {
    "imdb": 30,
    "mine": 25,
    "director": "Spielberg",
}
print(d)
print(d.keys())
print(d.values())
print(d["director"])

MOVIE_FN = "myData.csv"
my_csv = open(os.path.join(DATA_DIR, MOVIE_FN), "r", encoding="utf-8")
lines = my_csv.readlines()
rows = [line.split(";") for line in lines]

headers = rows[0][0]
headers = [header[1:-1] for header in headers.split(",")]
print(headers)

del rows[0]
rows = [row[0].split(",") for row in rows]

cols = {}
for i in range(len(headers)):
    header = headers[i]
    cols[header] = []
    for row in rows:
        cols[header].append(row[i])

print(cols.keys())
print(cols["imdb"][1:3])
print(cols["URL"])
my_csv.close()
